<?php

namespace App\DataFixtures;

use App\Entity\Evenement;
use App\Entity\Lieu;
use App\Entity\Participant;
use App\Repository\EvenementRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Faker\Provider\Address;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // use the factory to create a Faker\Generator instance
        $faker = Faker\Factory::create('fr_FR');

        $evenements = [];

        for ($i = 0; $i < 20; $i++) {
            $lieu = new Lieu($faker->word(), $faker->address(), Address::postcode(), $faker->city());
            $manager->persist($lieu);

            $evenement = new Evenement($faker->word(), $faker->words(6, true), "https://picsum.photos/200/300");
            $evenement->setLieu($lieu);
            $evenements[] = $evenement;
            $manager->persist($evenement);
        }

        for ($i = 0; $i < 50; $i++) {
            $participant = new Participant();
            $participant->setName($faker->firstName());
            $participant->setLastname($faker->lastName());
            $participant->setEmail($faker->email());
            for ($y = 0; $y < rand(0, 3); $y++) {
                $evenement = $evenements[rand(0, count($evenements) - 1)];
                $participant->addEvenement($evenement);
            }
            $manager->persist($participant);
        }

        $manager->flush();
    }
}