<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Entity\Lieu;
use App\Form\EvenementType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private function createEvent(ManagerRegistry $doctrine)
    {
        $entityManager = $doctrine->getManager();

        $evenement = new Evenement("Premier evenement", "Et voici sa description", "https://picsum.photos/200/300", new \DateTime());



        $entityManager->persist($evenement);
        $entityManager->flush();

        return $evenement;
    }

    #[Route('/test/validation', name: 'app_homepage')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $evenement = $this->createEvent($doctrine);

        return $this->render('default/index.html.twig', [
            'evenement' => $evenement,
        ]);
    }

    #[Route('/', name: "index")]
    #[Route('/exo1bis', name: 'exo1_bis')]
    public function exo1bis(ManagerRegistry $doctrine): Response
    {
        $evenements = $doctrine->getRepository(Evenement::class)->findAll();

        return $this->render('exo1bis.html.twig', [
            'evenements' => $evenements,
        ]);
    }

    #[Route('/exo1bis/{id}', name: 'evt_detail')]
    public function evenementDetail(Evenement $evenement): Response
    {
        return $this->render('evenementDetail.html.twig', [
            'evenement' => $evenement,
        ]);
    }

    #[Route('/lieux', name: 'exo2')]
    public function exo2(ManagerRegistry $doctrine): Response
    {
        $lieux = $doctrine->getRepository(Lieu::class)->findAll();

        return $this->render('exo2.html.twig', [
            "lieux" => $lieux
        ]);
    }

    #[Route('/evenements-form', name: 'exo3')]
    public function exo3(): Response
    {

        $form = $this->createForm(EvenementType::class);

        return $this->renderForm('exo3.html.twig', [
            "form" => $form
        ]);
    }


    #[Route('/evenement/ajout', name: 'ajouter_evenement',  methods: ['POST'])]
    public function ajoutEvenement(Request $request, ManagerRegistry $doctrine): Response
    {
        $evenement = new Evenement();
        $entityManager = $doctrine->getManager();

        $form = $this->createForm(EvenementType::class, $evenement);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $evenement = $form->getData();

            $entityManager->persist($evenement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('exo3');
    }
}